<?php

return [
    'utf_mode' => [
        'value' => true,
        'readonly' => true,
    ],
    'cache_flags' => [
        'value' => [
            'config_options' => 3600.0,
            'site_domain' => 3600.0,
        ],
        'readonly' => false,
    ],
    'cookies' => [
        'value' => [
            'secure' => false,
            'http_only' => true,
        ]
    ],
    'exception_handling' => [
        'value' => [
            'debug' => true,
            'handled_errors_types' => 4437,
            'exception_errors_types' => 4437,
            'ignore_silence' => false,
            'assertion_throws_exception' => true,
            'assertion_error_type' => 256,
            'log' => NULL,
        ],
        'readonly' => false,
    ],
    'connections' => [
        'value' => [
            'default' => [
                'className' => '\\Bitrix\\Main\\DB\\MysqliConnection',
                'host' => 'mysql',
                'database' => 'bitrix_in_docker',
                'login' => 'root',
                'password' => 'root',
                'options' => 2.0,
            ]
        ],
        'readonly' => true,
    ],
    'crypto' => [
        'value' => [
            'crypto_key' => 'ee51ed9ab268cdb61ead6718c2b8cba3',
        ],
        'readonly' => true,
    ],
    'routing' => [
        'value' => ['config' => ['api.php']]
    ],
    'composer' => [
        'value' => [
            'config_path' => '/var/www/site/composer.json',
        ]
    ]
];
